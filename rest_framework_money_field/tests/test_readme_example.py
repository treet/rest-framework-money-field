import json

from moneyed import Money
from rest_framework.renderers import JSONRenderer
from rest_framework.serializers import Serializer

from .. import MoneyField


class ProductSerializer(Serializer):
    price = MoneyField()


def test_serializer():
    serializer = ProductSerializer(instance={"price": Money(50, "EUR")})

    result = json.loads(JSONRenderer().render(data=serializer.data))

    assert result == {
        "price": {
            "amount": 5000,
            "currency": "EUR",
        },
    }
