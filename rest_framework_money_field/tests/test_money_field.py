import pytest

from decimal import Decimal

from rest_framework.exceptions import ValidationError
from moneyed import Money

from .. import MoneyField, MoneyRepresentation


@pytest.mark.parametrize(
    ("amount", "currency", "expected_amount"),
    (
        (5000, "EUR", Decimal("50")),
        (5050, "USD", Decimal("50.5")),
        (100, "SEK", Decimal("1")),
        (123456, "JOD", Decimal("123.456")),
    ),
)
def test_to_internal_value(amount: int, currency: str, expected_amount: Decimal):
    data: MoneyRepresentation = {"amount": amount, "currency": currency}

    result = MoneyField().to_internal_value(data=data)

    assert isinstance(result, Money)
    assert result.amount == expected_amount
    assert result.currency.code == currency


@pytest.mark.parametrize("amount", (5000, 5050, 100))
def test_to_internal_value_with_missing_currency(amount: int):
    data = {"amount": amount}

    with pytest.raises(ValidationError):
        MoneyField().to_internal_value(data=data)  # type: ignore


def test_to_internal_value_with_null_data():
    assert MoneyField().to_internal_value(data=None) is None


def test_to_internal_value_with_money_instance():
    data = Money(amount=50, currency="USD")

    assert MoneyField().to_internal_value(data) == data


def test_to_internal_value_with_malformed_data():
    with pytest.raises(ValidationError):
        MoneyField().to_internal_value({"foo": "bar"})


def test_to_internal_value_with_unrecognized_currency():
    with pytest.raises(ValidationError):
        MoneyField().to_internal_value({"amount": 100, "currency": "NON-EXISTENT"})


@pytest.mark.parametrize(
    ("amount", "currency", "expected_amount"),
    (
        (50.0, "EUR", 5000),
        (50.5, "USD", 5050),
        (1.0, "SEK", 100),
        (123.456, "JOD", 123456),
    ),
)
def test_to_representation(amount: float, currency: str, expected_amount: int):
    value = Money(amount=amount, currency=currency)

    result = MoneyField().to_representation(value=value)

    assert isinstance(result, dict)
    assert result == {"amount": expected_amount, "currency": currency}


def test_to_representation_with_null_value():
    assert MoneyField().to_representation(value=None) is None
