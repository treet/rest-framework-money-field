import os
import sys

import django

from django.conf import settings


def pytest_configure():
    BASE_DIR = os.path.abspath(os.path.dirname(__file__))

    sys.path.append(os.path.abspath(os.path.join(BASE_DIR, "..", "..")))

    settings.configure(
        SECRET_KEY="test-key",
        DEBUG=False,
        TEMPLATE_DEBUG=False,
        ALLOWED_HOSTS=[],
        INSTALLED_APPS=(
            "django.contrib.admin",
            "django.contrib.auth",
            "django.contrib.contenttypes",
            "django.contrib.sessions",
            "django.contrib.messages",
            "django.contrib.staticfiles",
            "rest_framework_money_field",
        ),
        MIDDLEWARE_CLASSES=(
            "django.middleware.security.SecurityMiddleware",
            "django.contrib.sessions.middleware.SessionMiddleware",
            "django.middleware.common.CommonMiddleware",
            "django.middleware.csrf.CsrfViewMiddleware",
            "django.contrib.auth.middleware.AuthenticationMiddleware",
            "django.contrib.messages.middleware.MessageMiddleware",
            "django.middleware.clickjacking.XFrameOptionsMiddleware",
        ),
        ROOT_URLCONF="tests.urls",
        DATABASES={
            "default": {
                "ENGINE": "django.db.backends.sqlite3",
                "NAME": os.path.join(BASE_DIR, "test_db.sqlite3"),
            }
        },
        LANGUAGE_CODE="en-us",
        TIME_ZONE="UTC",
        USE_I18N=True,
        USE_L10N=True,
        USE_TZ=True,
        STATIC_URL="/static/",
    )

    django.setup()
